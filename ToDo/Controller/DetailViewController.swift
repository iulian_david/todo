//
//  DetailViewController.swift
//  ToDo
//
//  Created by iulian david on 2/26/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Contacts

class DetailViewController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    
    var itemInfo: (ItemManager, Int)?

    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DATE_FORMAT
        return dateFormatter
    }()
    
    override func viewDidAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
        mapView.delegate = self
        let userLocation = mapView.userLocation
        
        
        guard let itemInfo = itemInfo
            else { fatalError() }
        let item = itemInfo.0.item(at: itemInfo.1)
        titleLabel.text = item.title
        locationLabel.text = item.location?.name
        descriptionLabel.text = item.itemDescription
        if let timestamp = item.timestamp {
            let date = Date(timeIntervalSince1970: timestamp)
            dateLabel.text = dateFormatter.string(from: date)
        }
        if let coordinate = item.location?.coordinate {
            let region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate,100, 100)
            mapView.region = region
            mapView.showsUserLocation = true
            mapView.mapType = MKMapType.standard
            
            
            if #available(iOS 10.0, *) {
                let placemark = MKPlacemark(coordinate: coordinate)
                let mapItem = MKMapItem(placemark: placemark)
                let options = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
                
                let annotation = MKPointAnnotation()
                annotation.coordinate = coordinate
                annotation.title = item.location?.name
                
                mapView.addAnnotation(annotation)
                
                mapView.showsTraffic = true
            } else {
                // Fallback on earlier versions
            };
            
            
        }
    }
    
    func checkItem() {
        if let itemInfo = itemInfo {
            itemInfo.0.checkItem(at: itemInfo.1)
        }
    }
}

