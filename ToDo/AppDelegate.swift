//
//  AppDelegate.swift
//  ToDo
//
//  Created by iulian david on 2/24/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var locationManager: CLLocationManager?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        locationManager = CLLocationManager()
        locationManager?.requestWhenInUseAuthorization()
        return true
    }

   

}

