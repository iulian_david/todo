//
//  ItemManager.swift
//  ToDo
//
//  Created by iulian david on 2/24/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit

class ItemManager: NSObject {
    
    
    /// Register to UIApplicationWillResignActive notification
    /// and save the items when this is triggerred
    override init() {
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(save), name: .UIApplicationWillResignActive, object: nil)
        
        ///read the data from toDoItems.plist if any
        if let nsToDoItems = NSArray(contentsOf : toDoPathURL) {
            for dict in nsToDoItems {
                if let toDoItem = ToDoItem(dict: dict as! [String: Any]) {
                    toDoItems.append(toDoItem)
                }
            }
        }
    }
    
    var  toDoCount: Int {
        return toDoItems.count
    }
    
    var  doneCount: Int {
        return doneItems.count
    }
    private var toDoItems: [ToDoItem] = []
    
    private var doneItems: [ToDoItem] = []
    
    func add(_ item: ToDoItem)  {
        
        if !toDoItems.contains(item) {
            toDoItems.append(item)
        }
    }
    
    func item(at index: Int) -> ToDoItem {
        return toDoItems[index]
    }
    
    func checkItem(at index: Int) {
        
        let doneItem = toDoItems.remove(at: index)
        doneItems.append(doneItem)
    }
    
    func uncheckItem(at index: Int) {
        
        let doneItem = doneItems.remove(at: index)
        toDoItems.append(doneItem)
    }
    
    func doneItem(at index: Int) ->ToDoItem {
        return doneItems[index]
    }
    
    func removeAll() {
        toDoItems.removeAll()
        doneItems.removeAll()
    }
    
    ///The URL 
    var toDoPathURL : URL {
        let fileURLs = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        
        guard let documetsURL = fileURLs.first else {
            print("Something went wrong. Documents url could not be found")
            fatalError()
        }
        
        return documetsURL.appendingPathComponent("toDoItems.plist")
    }
    
    
    /// saves the items to disk
    func save(){
        let nsToDoItems = toDoItems.map { $0.plistDict }
        
        guard nsToDoItems.count > 0 else {
            try? FileManager.default.removeItem(at: toDoPathURL)
            
            return
        }
        
        do {
            let plistData = try PropertyListSerialization.data(fromPropertyList: nsToDoItems, format: PropertyListSerialization.PropertyListFormat.xml, options: PropertyListSerialization.WriteOptions(0))
            
            try plistData.write(to: toDoPathURL, options: Data.WritingOptions.atomic)
        } catch  {
            print(error)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        save()
    }
}

///we need to declare the protocol to be @objc because we've set the data provider from the storyboard
@objc protocol ItemManagerSettable {
    var itemManager: ItemManager? { get set }
}
