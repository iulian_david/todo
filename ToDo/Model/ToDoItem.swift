//
//  ToDoItem.swift
//  ToDo
//
//  Created by iulian david on 2/24/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import Foundation


struct ToDoItem {
    
    let title:  String
    let itemDescription: String?
    let timestamp: Double?
    let location: Location?
    
    init(title: String, itemDescription: String? = nil, timestamp: Double? = nil,
         location: Location? = nil) {
        self.title = title
        self.itemDescription = itemDescription
        self.timestamp = timestamp
        self.location = location
    }
    /// The data model we have implemented uses structs. Unfortunately, structs cannot be written to a plist. We have to convert the data into Any arrays and String:Any dictionaries.
    private let titleKey = "titleKey"
    private let itemDescriptionKey = "itemDescriptionKey"
    private let timestampKey = "timestampKey"
    private let locationKey = "locationKey"
    var plistDict: [String: Any] {
        var dict = [String: Any]()
        dict[titleKey] = title
        
        if let itemDescription = itemDescription {
            dict[itemDescriptionKey] = itemDescription
        }
        
        if let timestamp = timestamp {
            dict[timestampKey] = timestamp
        }
        
        if let location = location {
            let locationDict = location.pListDict
            dict[locationKey] = locationDict
        }
        return dict
    }
    
    init?(dict : [String: Any]) {
        guard let title = dict[titleKey] as? String else { return nil }
        
        self.title = title
        self.itemDescription = dict[itemDescriptionKey] as? String
        self.timestamp = dict[timestampKey] as? Double
        
        if let locationDict = dict[locationKey] as? [String: Any] {
            self.location = Location(dict: locationDict)
        } else {
            self.location = nil
        }
    }
}

extension ToDoItem: Equatable {
    
    /// Returns a Boolean value indicating whether two values are equal.
    ///
    /// Equality is the inverse of inequality. For any values `a` and `b`,
    /// `a == b` implies that `a != b` is `false`.
    ///
    /// - Parameters:
    ///   - lhs: A value to compare.
    ///   - rhs: Another value to compare.
    public static func ==(lhs: ToDoItem, rhs: ToDoItem) -> Bool {
        
        if lhs.location != rhs.location {
            return false
        }
        
        if lhs.timestamp != rhs.timestamp {
            return false
        }
        
        if lhs.itemDescription != rhs.itemDescription {
            return false
        }
        
        if lhs.title != rhs.title {
            return false
        }
        return true
    }
}

