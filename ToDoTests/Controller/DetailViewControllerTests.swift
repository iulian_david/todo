//
//  DetailViewControllerTests.swift
//  ToDo
//
//  Created by iulian david on 2/26/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import XCTest
@testable import ToDo
import CoreLocation


class DetailViewControllerTests: XCTestCase {
    
    var storyboard: UIStoryboard!
    var sut: DetailViewController!
    
    override func setUp() {
        super.setUp()
        
        storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        
        _ = sut.view
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_HasTitleLabel() {
       
        XCTAssertNotNil(sut.titleLabel)
    }
    
    func test_HasDateLabel() {
        
        XCTAssertNotNil(sut.dateLabel)
    }
    
    
    func test_HasDescriptionLabel() {
        
        XCTAssertNotNil(sut.descriptionLabel)
    }
    
    func test_HasLocationLabel() {
        
        XCTAssertNotNil(sut.locationLabel)
    }
    
    func test_HasMapView() {
        XCTAssertNotNil(sut.mapView)
    }
    
    
    func test_SettingItemInfo_SetsTextsToLabels() {
        let coordinate = CLLocationCoordinate2DMake(51.2277, 6.7735)
        let location = Location(name: "Foo", coordinate: coordinate)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DATE_FORMAT
        let expectedDateText = dateFormatter.string(from: Date())
        
        let item = ToDoItem(title: "Bar", itemDescription: "Baz", timestamp: Date().timeIntervalSince1970, location: location)
        let itemManager = ItemManager()
        itemManager.add(item)
        sut.itemInfo = (itemManager, 0)
        
        sut.beginAppearanceTransition(true, animated: true)
        sut.endAppearanceTransition()
        XCTAssertEqual(sut.titleLabel.text, "Bar")
        XCTAssertEqual(sut.dateLabel.text, expectedDateText)
        XCTAssertEqual(sut.locationLabel.text, "Foo")
        XCTAssertEqual(sut.descriptionLabel.text, "Baz")
        XCTAssertEqualWithAccuracy(sut.mapView.centerCoordinate.latitude, coordinate.latitude, accuracy: 0.001)
        XCTAssertEqualWithAccuracy(sut.mapView.centerCoordinate.longitude, coordinate.longitude, accuracy: 0.001)
    }
    
    
    func test_CheckItem_ChecksItemInItemManager() {
        let itemManager = ItemManager()
        itemManager.add(ToDoItem(title: "Foo"))
        sut.itemInfo = (itemManager, 0)
        sut.checkItem()
        XCTAssertEqual(itemManager.toDoCount, 0)
        XCTAssertEqual(itemManager.doneCount, 1)
    }
}
