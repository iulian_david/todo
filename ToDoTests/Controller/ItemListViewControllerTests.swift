//
//  ItemListViewControllerTest.swift
//  ToDo
//
//  Created by iulian david on 2/25/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import XCTest
@testable import ToDo

class ItemListViewControllerTest: XCTestCase {
    
    var sut: ItemListViewController!
    
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ItemListViewController")
        sut = viewController as! ItemListViewController
        _ = sut.view
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_TableViewIsNotNilAfterViewDidLoad() {
        
        XCTAssertNotNil(sut.tableView)
    }
    
    func test_LoadingView_SetsTableViewDataSource() {
       
        XCTAssertTrue(sut.tableView.dataSource is ItemListDataProvider)
    }
    
    func test_LoadingView_SetsTableViewDelegate() {
        
        XCTAssertTrue(sut.tableView.delegate is ItemListDataProvider)
    }
    
    func test_LoadingView_SetsDataSourceAndDelegateToSameObject() {
        XCTAssertEqual(sut.tableView.dataSource as? ItemListDataProvider,
                       sut.tableView.delegate as? ItemListDataProvider)
    }
    
    func test_ItemListViewController_HasAddBarButtonWithSelfAsTarget() {
        let target = sut.navigationItem.rightBarButtonItem?.target
        XCTAssertEqual(target as? UIViewController, sut)
    }
    
    func test_AddItem_PresentsAddItemViewController() {
        guard let inputViewController = pressAddItemButton() else {
            XCTFail()
            return
        }
        
        XCTAssertNotNil(inputViewController.titleTextField)
    }
    
    
    func testItemListVC_SharesItemManagerWithInputVC() {
        
        guard let inputViewController = pressAddItemButton() else {
            XCTFail()
            return
        }
        
        
        guard let inputItemManager = inputViewController.itemManager else
        { XCTFail(); return }
        XCTAssertTrue(sut.itemManager === inputItemManager)
    }
    
    
    
    private func pressAddItemButton() -> InputViewController? {
        XCTAssertNil(sut.presentedViewController)
        
        guard let addButton = sut.navigationItem.rightBarButtonItem else {
            XCTFail();
            return nil
        }
        guard let action = addButton.action else {
            XCTFail();
            return nil
        }
        
        ///It is only possible to present a View Controller from another View Controller whose view is in the view hierarchy.
        UIApplication.shared.keyWindow?.rootViewController = sut
        
        sut.performSelector(onMainThread: action, with: addButton, waitUntilDone: true)
        
        XCTAssertNotNil(sut.presentedViewController)
        XCTAssertTrue(sut.presentedViewController is InputViewController)
        guard let inputViewController =
            sut.presentedViewController as? InputViewController else {
            XCTFail();
            return nil
        }
        
        return inputViewController
        
    }
    
    func test_ViewDidLoad_SetsItemManagerToDataProvider() {
        XCTAssertTrue(sut.itemManager === sut.dataProvider.itemManager)
    }
    
    func test_TableView_ShouldReloadWhenItemIsAdded() {
        
        let mockTableView = MockTableView.mockTableView(withDataSource: sut.dataProvider)
        
        sut.tableView = mockTableView
        XCTAssertNotNil(sut.tableView)
        
        let first = item()
        sut.itemManager.add(first)
        
        sut.beginAppearanceTransition(true, animated: true)
        sut.endAppearanceTransition()
        
        XCTAssertTrue(mockTableView.reloadDataGotCalled)

        
        let cell1 = mockTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! MockItemCell
        XCTAssertEqual(cell1.catchedItem, first)
        
    }
    
    private func item(_ title: String = "Foo") -> ToDoItem {
        return ToDoItem(title: title)
    }
    
    // MARK - Showing the detail view
    func testItemSelectedNotification_PushesDetailVC() {
        let mockNavigationController = MockNavigationController(rootViewController: sut)
        
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
        
        _ = sut.view
        
        NotificationCenter.default.post(name: NSNotification.Name("ItemSelectedNotification"), object: self, userInfo: ["index": 1])
        
        guard let detailViewController = mockNavigationController.pushedViewController as? DetailViewController else { XCTFail(); return }
        
        guard let detailItemManager = detailViewController.itemInfo?.0  else { XCTFail(); return }
        
        guard let index = detailViewController.itemInfo?.1  else { XCTFail(); return }
        
        _ = detailViewController.view
        
        XCTAssertNotNil(detailViewController.titleLabel)
        XCTAssertTrue(detailItemManager === sut.itemManager)
        XCTAssertEqual(index, 1)
    }
}

///Mocking tableView, first to reload data, then to test
///that the created created cell has the item provided
extension ItemListViewControllerTest {
    
    class MockTableView: UITableView {
        var reloadDataGotCalled = false
        var cellGotDequeued = false
        override func dequeueReusableCell( withIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
            cellGotDequeued = true
            return super.dequeueReusableCell(withIdentifier: identifier,for: indexPath)
        }
        override func reloadData() {
            reloadDataGotCalled = true
            super.reloadData()
        }
        class func mockTableView(withDataSource dataSource: UITableViewDataSource) -> MockTableView {
            let mockTableView = MockTableView( frame: CGRect(x: 0, y: 0, width: 320, height: 480), style: .plain)
            mockTableView.dataSource = dataSource
            mockTableView.register(MockItemCell.self, forCellReuseIdentifier: "ItemCell")
            return mockTableView
        }
    }
    
    class MockItemCell : ItemCell {
        
        var catchedItem: ToDoItem?
        
        override func configCell(with item: ToDoItem, checked: Bool = false) {
            catchedItem = item
        }
    }
}

// MARK - Showing the detail view

extension ItemListViewControllerTest {
    /// This is a mock for UINavigationController, and it simply registers when a View Controller is pushed onto the navigation stack
    class MockNavigationController : UINavigationController {
        var pushedViewController: UIViewController?
        override func pushViewController(_ viewController: UIViewController, animated: Bool) {
            pushedViewController = viewController
            super.pushViewController(viewController, animated: animated)
        }
    }
}
